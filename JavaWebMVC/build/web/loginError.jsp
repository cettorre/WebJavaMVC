<%-- 
    Document   : loginError
    Created on : 19-dic-2017, 22.50.59
    Author     : pierf
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Error try again</h1>
        <%@include file="WEB-INF/jspf/loginform.jspf" %>
    </body>
</html>
